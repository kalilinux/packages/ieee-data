# Copyright © 2013 Filippo Giunchedi <filippo@debian.org>
# Copyright © 2014 Luciano Bello <luciano@debian.org>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar.
# See the LICENSE file for more details.

sharedir=/usr/share/ieee-data
vardir=/var/lib/ieee-data
bindir=/usr/bin
mandir=/usr/share/man
all:

install:
	install -D -m644 oui.txt $(DESTDIR)/$(sharedir)/oui.txt
	install -D -m644 iab.txt $(DESTDIR)/$(sharedir)/iab.txt
	install -D -m644 .lastupdate $(DESTDIR)/$(sharedir)/.lastupdate
	install -D -m644 update-oui.8 $(DESTDIR)/$(mandir)/man8/update-oui.8
	install -D -m755 update-oui $(DESTDIR)/$(bindir)/update-oui

update:
	BASEDIR=. RUN_PARSERS=0 ./update-oui -f
